# UniHelp: Universal Helper #

## Description ##
 
  This program completely describes own name. This is a small console task tracker. Includes the functions of the calendar and to-do list.
  The application is divided into two parts:
  
  1. Library for working with tasks, tags, notifications, task planners, database. After installing the library, you can import it for work:
  ```import unihelplib```
  
  2. Console represents the library support in the console application. For the multi-user mode, the User class is implemented.
  
## Installation ##
  
 1. If you don't have setuptools: 
 ```bash $ pip3 install -U pip setuptools ``` 
  
 2. Installing unihelplib (library): 
 ```bash $ cd library $ python3 setup.py install ``` 
  
 3. Running tests: 
 ```bash $ python3 setup.py test ```
  
 4. Installing unihelp (console):
 ```bash $ cd console $ python3 setup.py install ```
  
## Manual ##
  The most important advice when using unihelp to get help enter:
  ```bash $ unihelp -h``` or ```bash $ unihelp --help```
  
  Application template ```unihelp <object> [<args>]```
  
  Objects: user, task, tag, notification, planner.
  
  ```bash $ unihelp user [command]```
  
  ```bash $ unihelp tag [command]```
  
  To see available commands: ```bash $ unihelp planner -h```

## Configurations ##

In the console.command_line.config.py you can change the path and name of the database and the logging settings.

 DIRECTORY_APP - application creates a folder
 for storing information about the user,
 database, log;

 DEFAULT_DATABASE - file database;

 LOGGING_ENABLED - is logger work;
 
 IS_STDOUT - is logger print in stdout not in file;

 LOG_LEVEL - level of logging (DEBUG/INFO/ERROR);

 LOGS_DIRECTORY - directory of logging;

 LOG_FILE - where the logger writes its data;

 LOG_FORMAT - format logging.
