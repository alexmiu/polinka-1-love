"""This module is needed for error handling.

    class InvalidTaskTime - task start time later than task end time.
    A task can not start later than it ends;
    class InvalidTaskInterval - too short a task interval, minimum interval - 60 seconds.
    Set interval less than a minute is illogical;
    TaskDoesntExist - access to the task which doesnt exist;
    AccessRights - someone is trying to do something that has
    no rights.
"""


class InvalidTaskTimeError(Exception):
    """Time incorrect exception.

    Task start time later than task end time.
    A task can not start later than it ends"""

    def __init__(self, time_start, time_end):
        super().__init__(
            "The task start time {} is longer than the end time {}".format(
                time_start, time_end))


class InvalidTaskIntervalError(Exception):
    """Interval exception.

    Too short a task interval, minimum interval - 60 seconds.
    Set interval less than a minute is illogical."""

    def __init__(self, interval):
        super().__init__("The interval {} should be longer than 60 seconds".format(interval))


class TaskDoesntExistError(Exception):
    """Task doesn't exist.

    Access to the task which doesnt exist.
    """
    def __init__(self):
        super().__init__("Task doesn't exist.")


class AccessRightsError(Exception):
    """Rights exception.

    Someone is trying to do something that has
    no rights.
    """
    def __init__(self):
        super().__init__("User doesn't have rights to do this.")
