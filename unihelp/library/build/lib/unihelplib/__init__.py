"""Is a library for the work of the tracker.

Includes modules:

    actions.py - this module describes the interaction
    with the unihelplibrary and the logger; describes
    possible interactions between library models.

    Initialization:
    >>> from unihelplib.actions import Actions
    >>> actions = Actions(user_id=2, db_name="./unihelp.db")

    Basic work with items in database:
    >>> actions.add_tag(tag)
    >>> actions.delete_tag(tag.id)

    >>> actions.add_task(task)
    >>> actions.create_subtask(task.id, subtask)
    >>> actions.delete_task(task.id)

    >>> actions.add_planner(planner)
    >>> actions.delete_planner(planner.id)

    >>> actions.add_notification(notification)
    >>> actions.delete_notification(notification.id)

    Work  with rights for user to task:
    >>> actions.add_reader(user_id=1, task=task.id)
    >>> actions.add_writer(user_id=1, task=task.id)
    >>> actions.delete_reader(user_id=1, task=task.id)
    >>> actions.delete_writer(user_id=1, task=task.id)
    This commands work with current user. User who initialized
    in Actions(user_id=2).
    >>> actions.check_user_is_reader(task=task.id)
    >>> actions.check_user_is_writer(task=task.id)

    >>> actions.search_notification(notification.id)
    >>> actions.update_task(task)

    logger.py - module includes functions for work with logging;
    setup_logger(enabled, log_level, logging_path, log_format)
    - creates logger with preset settings; get_logger() - returns
    logger.

Includes packages:
    exceptions_____
    |exceptions.py -
    This module is needed for error handling.

    class InvalidTaskTime - task start time later than task end time.
    A task can not start later than it ends;
    class InvalidTaskInterval - too short a task interval, minimum interval - 60 seconds.
    Set interval less than a minute is illogical;
    TaskDoesntExist - access to the task which doesnt exist;
    AccessRights - someone is trying to do something that has
    no rights.

    models____
    |models.py -
    This module creates previously described entities in the database.

    Class of Notification model.
        This class is used to create notifications.
        The notification class has the same state field.
        A state enumeration describes the interaction between
        the notification and the user.

        >>> from unihelplib.models.models import Notification
        >>> import datetime
        >>> notification = Notification(
        ... title="title",
        ... task=task,
        ... user_id=2,
        ... work_start_time=datetime.datetime.now(),
        ... state=0)

        state - unihelplib.models.models.State

    Class of Planner model.
        This class is used to create planners.
        Task have Status REPEAT to work with Planner.

        >>> from unihelplib.models.models import Planner
        >>> planner = Planner(
        ... user_id=2,
        ... task=3,
        ... interval=300,
        ... last_repeat=datetime.datetime.now())

    Class of task's tag.
        The tag is an identifier for categorizing,
        describing, searching data and specifying
        an internal structure.
        >>> from unihelplib.models.models import Tag
        >>> tag = Tag(name="name", user_id=2)


    Class of Task model.
        This class is used to create task.
        The Task class has the same status field.
        A status enumeration describes the interaction between
        the task and the user.
        The Task class has the same priority field.
        A priority enumeration describes importance task.

        >>> from unihelplib.models.models import Task
        >>> task = Task(
        ... title="title",
        ... note="note",
        ... time_start=datetime.datetime.now(),
        ... time_end=datetime.datetime(year=2018, month=11, day=5),
        ... time_create=datetime.datetime.now(),
        ... time_update=datetime.datetime.now(),
        ... user_id=2,
        ... planner_id=2,
        ... parent_task_id=2,
        ... tag=2,
        ... is_event=True,
        ... priority=1,
        ... status=0)

        priority - unihelplib.models.models.Priority
        status - unihelplib.models.models.Status

    storage____
    |test.py -
    This module is a mediator for models in sqlitebase.

    >>> from unihelplib.storage.mediator import TagMediator, TaskMediator
    >>> tag_mediator = TagMediator(user_id=2, db_name="./unihelp.db")
    >>> tag = tag_mediator.create(tag)
    >>> tag_mediator.delete(tag.id)
    >>> tag = tag_mediator.search(tag.id)
    >>> task_mediator = TaskMediator(user_id=2, db_name="./unihelp.db")
    >>> task = task_mediator.create(task)
    >>> subtask = task_mediator.create(task)
    The library does not have a user model because
    the user is defined at the console or web application
    level. For example, the task should know about its creator
    or partner. So BaseMediator need param user_id.
    To work with the base optional need param database_name.
"""