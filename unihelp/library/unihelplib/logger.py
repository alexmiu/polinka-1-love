"""Module includes functions for work with logging.

setup_logger(enabled, log_level, logging_path, log_format) - creates logger with preset settings;
get_logger() - returns logger.

"""
import logging
import os
import sys


def setup_logger(
        enabled=True,
        log_level=logging.DEBUG,
        handler_path="./logging.log",
        is_stdout=False,
        log_format="%(asctime)s - %(name)s - [%(levelname)s] - %(message)s"):
    """Gets the logger by name, sets the logging settings: level, file, format, and turns it on or off.

        :param enabled: the logger is on or off (bool, default=True)
        :param log_level: with what level to log: 'DEBUG','INFO',WARNING','ERROR','CRITICAL' (string, default="DEBUG")
        :param handler_path: path to the file in which the logs are written (string, default="./logging/logging.log")
        :param log_format: format of the log line (string, default="time - name - [level] - message")
    """
    if not os.path.exists(os.path.dirname(handler_path)):
        os.makedirs(os.path.dirname(handler_path))

    logger = get_logger()
    if logger.hasHandlers():
        logger.handlers.clear()
    formatter = logging.Formatter(log_format)
    if not is_stdout:

        file_handler = logging.FileHandler(handler_path)

    else:
        file_handler = logging.StreamHandler(sys.stdout)

    file_handler.setLevel(log_level)
    file_handler.setFormatter(formatter)

    if not enabled:
        logger.disabled = True
        return

    logger.disabled = False
    logger.addHandler(file_handler)
    logger.setLevel(log_level)


def get_logger():
    """Returns logger with name 'unihelp_logger'."""
    return logging.getLogger("unihelp_logger")
