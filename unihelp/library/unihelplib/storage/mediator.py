"""This module is a mediator for models in sqlitebase.

The library does not have a user model because
the user is defined at the console or web application
level. For example, the task should know about its creator
or partner. So BaseMediator need param user_id.
To work with the base optional need param database_name.
"""
import datetime
import sqlite3
import pytz
import dateparser
import dateutil.parser
from dateutil.tz import tzlocal
from collections import deque
from unihelplib.models.models import (
    Tag,
    Task,
    State,
    Status,
    Planner,
    Notification,
    CreateTables)


class BaseMediator:
    def __init__(self, user_id, db_name="./unihelplib.db"):
        self.user_id = user_id
        self.db_name = db_name

        tables = CreateTables(db_name)
        self.cursor = tables.cursor
        self.conn = tables.conn
        self.conn.row_factory = sqlite3.Row

        sql = """SELECT COUNT (*)
            FROM sqlite_master WHERE type='table'
            AND name='{table_name}'""".format(table_name="Tag")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_tag()

        sql = """SELECT COUNT (*)
                    FROM sqlite_master WHERE type='table'
                    AND name='{table_name}'""".format(table_name="Notification")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_notification()

        sql = """SELECT COUNT (*)
                    FROM sqlite_master WHERE type='table'
                    AND name='{table_name}'""".format(table_name="Planner")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_planner()

        sql = """SELECT COUNT (*)
                    FROM sqlite_master WHERE type='table'
                    AND name='{table_name}'""".format(table_name="ReadUserRight")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_read_user_right()

        sql = """SELECT COUNT (*)
                    FROM sqlite_master WHERE type='table'
                    AND name='{table_name}'""".format(table_name="Task")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_task()

        sql = """SELECT COUNT (*)
                    FROM sqlite_master WHERE type='table'
                    AND name='{table_name}'""".format(table_name="WriteUserRight")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_write_user_right()

    def counter_table(self, Table):
        self.cursor.execute(
            """SELECT COUNT(*) FROM {Table}""".format(Table=Table))
        result = self.cursor.fetchone()[0]
        return result if result is not None else 0

class TagMediator(BaseMediator):
    def create(self, tag):
        """Creat tag for user in database."""
        tag.user_id = self.user_id
        sql = """INSERT INTO Tag (name, user_id)
         VALUES ('{name}',
                {user_id})""".format(name=tag.name, user_id=tag.user_id)
        self.cursor.execute(sql)
        self.conn.commit()

        tag.id = self.cursor.lastrowid
        return tag

    def delete(self, tag_id):
        """Delete tag from database."""
        sql = "DELETE from Tag WHERE id={tag_id}".format(tag_id=tag_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def update(self, tag):
        """Update tag in database."""
        sql = """UPDATE Tag SET name='{name}'
                WHERE id={tag_id}""".format(name=tag.name, tag_id=tag.id)
        self.cursor.execute(sql)
        self.conn.commit()
        return tag

    def search(self, tag_id):
        """Search tag by id."""
        sql = "SELECT * FROM Tag" \
              " WHERE id={tag_id}".format(tag_id=tag_id)
        try:
            self.cursor.execute(sql)
            result = self.cursor.fetchone()
        except Exception:
            return None
        return Tag(id=result['id'],
                   name=result['name'],
                   user_id=result['user_id'])

    def search_all(self):
        """Search all tags for user."""
        sql = """SELECT * FROM Tag
         WHERE user_id={user_id}""".format(user_id=self.user_id)
        tags = []
        for row in self.cursor.execute(sql):
            tags.append(Tag(id=row['id'],
                            name=row['name'],
                            user_id=row['user_id']))
        return tags


class NotificationMediator(BaseMediator):
    def create(self, notification):
        """Create notification for user in database."""
        notification.user_id = self.user_id

        sql = """INSERT INTO Notification (title, state,
            user_id, task_id, work_start_time) 
            VALUES ('{title}', {state}, {user_id},
            {task_id}, '{work_start_time}')""".format(
            title=notification.title,
            state=notification.state,
            user_id=notification.user_id,
            task_id=0 if notification.task_id is None else notification.task_id,
            work_start_time=str(notification.work_start_time))
        self.cursor.execute(sql)
        self.conn.commit()

        notification.id = self.cursor.lastrowid
        return notification

    def delete(self, notification_id):
        """Delete notification from database."""
        sql = """DELETE from Notification
         WHERE id={notification_id}""".format(notification_id=notification_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def update(self, notification):
        """Update notification in database."""
        sql = """UPDATE Notification SET title='{title}',
            state={state}, user_id={user_id}, task_id={task_id},
            work_start_time='{work_start_time}'
            WHERE id={notification_id}""".format(
            title=notification.title,
            state=notification.state,
            user_id=notification.user_id,
            task_id=0 if notification.task_id is None else notification.task_id,
            work_start_time=str(notification.work_start_time),
            notification_id=notification.id)

        self.cursor.execute(sql)
        self.conn.commit()
        return notification

    def search(self, notification_id):
        """Search notification by id."""
        sql = """SELECT * FROM Notification
            WHERE id={notification_id}""".format(notification_id=notification_id)
        self.cursor.execute(sql)
        notification = self.cursor.fetchone()
        if not notification:
            return None
        return Notification(id=notification['id'],
                            title=notification['title'],
                            task_id=notification['task_id'],
                            user_id=notification['user_id'],
                            work_start_time=dateparser.parse(
                                notification['work_start_time']),
                            state=notification['state'])

    def search_all(self):
        """Search all notifications for user."""
        sql = """SELECT * FROM Notification
            WHERE user_id={user_id}""".format(user_id=self.user_id)
        notifications = []
        for row in self.cursor.execute(sql):
            notifications.append(
                Notification(id=row['id'], title=row['title'],
                             task_id=row['task_id'],
                             user_id=row['user_id'],
                             work_start_time=dateparser.parse(
                                 row['work_start_time']),
                             state=row['state']))
        return notifications

    def search_all_by_state(self, state):
        """Search notification by param state.

        state value:
        CREATED = 0
        AWAIT = 1
        SHOWN = 2
        """
        sql = """SELECT * FROM Notification WHERE user_id={user_id} AND state={state}""".format(
            user_id=self.user_id, state=state.value)
        notifications = []
        for row in self.cursor.execute(sql):
            notifications.append(
                Notification(id=row['id'], title=row['title'],
                             task_id=row['task_id'],
                             user_id=row['user_id'],
                             work_start_time=dateparser.parse(
                                 row['work_start_time']),
                             state=row['state']))
        return notifications

    def set_state_shown(self, notification_id):
        """Make notification.state SHOWN."""
        notification = self.search(notification_id)
        notification.state = State.SHOWN.value
        self.update(notification)

    def progress(self):
        """Make progress for notifications state.CREATED->state.AWAIT."""
        sql = """SELECT * FROM Notification
            WHERE state={state}""".format(state=State.CREATED.value)
        for row in self.cursor.execute(sql):
            if dateparser.parse(row['work_start_time']) < datetime.datetime.now():
                state = State.AWAIT.value
                sql = """UPDATE Notification
                    SET state={state}
                    WHERE id={notification_id}""".format(state=state, notification_id=row['id'])
                self.cursor.execute(sql)
                self.conn.commit()


class PlannerMediator(BaseMediator):
    def create(self, planner):
        """Create planner for user in database."""
        planner.user_id = self.user_id
        sql = """INSERT INTO Planner (user_id, task_id,
            interval, last_repeat)
            VALUES ({user_id}, {task_id},
            {interval}, '{last_repeat}')""".format(
            user_id=planner.user_id,
            task_id=0 if planner.task_id is None else planner.task_id,
            interval=int(planner.interval),
            last_repeat=str(planner.last_repeat))
        self.cursor.execute(sql)
        self.conn.commit()

        planner.id = self.cursor.lastrowid
        return planner

    def delete(self, planner_id):
        """Delete planner from database."""
        sql = """DELETE from Planner
            WHERE id={planner_id}""".format(planner_id=planner_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def update(self, planner):
        """Update planner in database."""
        sql = """UPDATE Planner
            SET interval={interval}, last_repeat='{last_repeat}'
            WHERE id={planner_id}""".format(
            interval=planner.interval,
            last_repeat=str(planner.last_repeat),
            planner_id=planner.id)
        self.cursor.execute(sql)
        self.conn.commit()
        return planner

    def search(self, planner_id):
        """Search by id in database."""
        sql = """SELECT * FROM Planner
            WHERE id={planner_id}""".format(planner_id=planner_id)
        self.cursor.execute(sql)
        result = self.cursor.fetchone()
        if not result:
            return None
        task = None
        if result['task_id'] != 0:
            mediator = TaskMediator(user_id=self.user_id, db_name=self.db_name)
            task = mediator.search(result['task_id'])
        return Planner(
            id=result['id'],
            user_id=result['user_id'],
            task_id=task.id,
            interval=result['interval'],
            last_repeat=dateparser.parse(result['last_repeat']))

    def search_all(self):
        """Search all planners for user."""
        sql = """SELECT * FROM Planner
            WHERE user_id={user_id}""".format(user_id=self.user_id)
        planners = []
        for row in self.cursor.execute(sql):
            task = None
            if row['task_id'] != 0:
                mediator = TaskMediator(user_id=self.user_id, db_name=self.db_name)
                task = mediator.search(row['task_id'])
            planners.append(
                Planner(id=row['id'],
                        user_id=row['user_id'],
                        task_id=task.id,
                        interval=row['interval'],
                        last_repeat=dateparser.parse(row['last_repeat'])))
        return planners

    def progress(self, task_mediator):
        """Check and update time for periodic task."""
        for row in self.cursor.execute("SELECT * FROM Planner"):
            task = None
            if row['task_id'] != 0:
                task = task_mediator.search(row['task_id'])
            planner = Planner(id=row['id'],
                              user_id=row['user_id'],
                              task_id=task.id,
                              interval=int(row['interval']),
                              last_repeat=dateutil.parser.parse(row['last_repeat']).astimezone(tzlocal()))

            last_repeat = (planner.last_repeat + datetime.timedelta(seconds=planner.interval))

            now = pytz.UTC.localize(datetime.datetime.now())
            if last_repeat < now:
                try:
                    task = task_mediator.search(planner.task_id)
                    task.id = None
                    task.status = Status.IN_PROGRESS.value
                    task.planner_id = planner.id
                    task_mediator.create(task)
                    time = datetime.timedelta(seconds=(planner.interval))

                    while last_repeat < now:
                        last_repeat += time

                    planner.last_repeat = last_repeat
                    self.update(planner)
                except:
                    return None


class TaskMediator(BaseMediator):
    def create(self, task):
        """Create task for user in database."""
        task.user_id = self.user_id
        sql = """INSERT INTO Task (title, note, user_id,
            tag_id, planner_id, parent_task_id,
            time_start, time_end, time_create,
            time_update, is_event, priority, status) 
            VALUES ('{title}', '{note}', {user_id},
            {tag_id}, {planner_id}, {parent_task_id},
            '{time_start}', '{time_end}', '{time_create}',
            '{time_update}', {is_event}, {priority}, {status})""".format(
            title=task.title,
            note=task.note,
            user_id=task.user_id,
            tag_id=0 if task.tag_id is None else task.tag_id,
            planner_id=0 if task.planner_id is None else task.planner_id,
            parent_task_id=0 if task.parent_task_id is None else task.parent_task_id,
            time_start="" if task.time_start is None else str(task.time_start),
            time_end="" if task.time_end is None else str(task.time_end),
            time_create=str(task.time_create),
            time_update=str(task.time_update),
            is_event=int(task.is_event),
            priority=task.priority,
            status=task.status)
        self.cursor.execute(sql)
        self.conn.commit()

        task.id = self.cursor.lastrowid
        return task

    def delete(self, task_id):
        """Delete task from database."""
        sql = """DELETE from Task
            WHERE id={task_id}""".format(task_id=task_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def update(self, task):
        """Update task in database."""
        sql = """UPDATE Task
            SET title='{title}', note='{note}', user_id={user_id},
            tag_id={tag_id}, planner_id={planner_id},
            parent_task_id={parent_task_id}, time_start='{time_start}',
            time_end='{time_end}', time_create='{time_create}',
            time_update='{time_update}', is_event={is_event},
            priority={priority}, status={status}
            WHERE id={task_id}""".format(
            title=task.title,
            note=task.note,
            user_id=task.user_id,
            tag_id=0 if task.tag_id is None else task.tag_id,
            planner_id=0 if task.planner_id is None else task.planner_id,
            parent_task_id=0 if task.parent_task_id is None else task.parent_task_id,
            time_start="" if task.time_start is None else str(task.time_start),
            time_end="" if task.time_end is None else str(task.time_end),
            time_create=str(task.time_create),
            time_update=str(datetime.datetime.now()),
            is_event=int(task.is_event),
            priority=task.priority,
            status=task.status,
            task_id=task.id)

        self.cursor.execute(sql)
        self.conn.commit()
        return task

    def search(self, task_id):
        """Search task by id in database."""
        sql = """SELECT * FROM Task
            WHERE id={task_id}""".format(task_id=task_id)
        self.cursor.execute(sql)
        result = self.cursor.fetchone()
        if not result:
            return None
        return Task(id=result['id'],
                    title=result['title'],
                    note=result['note'],
                    user_id=result['user_id'],
                    tag_id=result['tag_id'] if result['tag_id'] != 0 else None,
                    planner_id=result['planner_id'],
                    parent_task_id=result['parent_task_id'],
                    time_start=dateparser.parse(result['time_start']),
                    time_end=dateparser.parse(result['time_end']),
                    time_create=dateparser.parse(result['time_create']),
                    time_update=dateparser.parse(result['time_update']),
                    is_event=bool(result['is_event']),
                    priority=result['priority'],
                    status=result['status'])

    def search_user_tasks(self):
        """Search all task for user."""
        sql = """SELECT * FROM Task
            WHERE user_id={user_id}""".format(user_id=self.user_id)
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def set_status(self, task_id, status):
        """Set status for task.

        param status:
        CREATED 0
        IN_PROGRESS 1
        DONE 2
        ARCHIVED 3
        REPEAT 4
        """
        task = self.search(task_id)
        task.status = status
        self.update(task)

    def search_by_status(self, status):
        """Get tasks by status.

        param status:
        CREATED 0
        IN_PROGRESS 1
        DONE 2
        ARCHIVED 3
        REPEAT 4
        """
        sql = """SELECT * FROM Task
            WHERE user_id={user_id},
            status={status}""".format(user_id=self.user_id, status=status)
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def create_subtask(self, parent_task_id, task):
        """Create task-children for task-parent in database."""
        task.parent_task_id = parent_task_id
        return self.create(task)

    def search_subtasks(self, task_id, recursive=False):
        """Search subtasks (single-recursive=False or
         all-recursive=True) for task."""
        if recursive:
            queue = deque()
            queue.append(self.search(task_id))
            subtasks = []
            return self.search_recursive_subtasks(queue, subtasks)
        sql = """SELECT * FROM Task
            WHERE parent_task_id={parent_task_id}""".format(
            user_id=self.user_id, parent_task_id=task_id)
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(id=row['id'],
                              title=row['title'],
                              note=row['note'],
                              user_id=row['user_id'],
                              tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                              planner_id=row['planner_id'],
                              parent_task_id=row['parent_task_id'],
                              time_start=dateparser.parse(row['time_start']),
                              time_end=dateparser.parse(row['time_end']),
                              time_create=dateparser.parse(row['time_create']),
                              time_update=dateparser.parse(row['time_update']),
                              is_event=bool(row['is_event']),
                              priority=row['priority'],
                              status=row['status']))
        return tasks

    def search_recursive_subtasks(self, queue, subtasks):
        """BFS - breadth-first search.

        Let's create a queue q, in which the burning
        vertices will be placed, and also we will create
        an array subtasks [], in which we will mark
        for each vertex whether it is already lit or
        not (or in other words, whether it was a visitor).
        """
        if not queue:
            return subtasks
        task = queue.popleft()
        for subtask in self.search_subtasks(task.id):
            subtasks.append(subtask)
            queue.append(subtask)
        return self.search_recursive_subtasks(queue, subtasks)

    def search_tasks_by_planner(self, planner_id):
        """Search cycle tasks in planner for user."""
        sql = """SELECT * FROM Task
            WHERE user_id={user_id},
            planner_id={planner_id}""".format(
            user_id=self.user_id, planner_id=planner_id)
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def check_is_reader(self, task_id):
        """Check right fot read task for user."""
        self.cursor.execute(
            """SELECT COUNT(*) FROM ReadUserRight
            WHERE user_id={user_id} and task_id={task_id}""".format(
                user_id=self.user_id, task_id=task_id))
        if self.cursor.fetchone()[0] == 1:
            return True
        return False

    def add_reader(self, user_id, task_id):
        """Add read right for task to user."""
        self.cursor.execute(
            """SELECT COUNT(*) FROM ReadUserRight
             WHERE user_id={user_id} and task_id={task_id}""".format(
                user_id=user_id, task_id=task_id))
        if self.cursor.fetchone()[0] == 0:
            sql = """INSERT INTO ReadUserRight (user_id, task_id) 
                VALUES ({user_id}, {task_id})""".format(
                user_id=user_id, task_id=task_id)
            self.cursor.execute(sql)
            self.conn.commit()

    def search_readers(self, task_id):
        """All readers task in database."""
        sql = """SELECT * FROM ReadUserRight
            WHERE task_id={task_id}""".format(task_id=task_id)
        users_id = []
        for row in self.cursor.execute(sql):
            users_id.append(row['id'])
        return users_id

    def delete_reader(self, user_id, task_id):
        """Delete read right for task."""
        sql = """DELETE from ReadUserRight
            WHERE user_id={user_id} and task_id={task_id}""".format(
            user_id=user_id, task_id=task_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def delete_all_readers(self, task_id):
        """Delete all readers for task."""
        sql = """DELETE from ReadUserRight
            WHERE task_id={task_id}""".format(task_id=task_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def search_read_tasks(self):
        """Get tasks for user who can read."""
        sql = """SELECT * FROM Task
            INNER JOIN ReadUserRight ON Task.id=ReadUserRight.task_id
            WHERE ReadUserRight.user_id={user_id}
            """.format(user_id=self.user_id)
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def check_is_writer(self, task_id):
        """Check right for write task."""
        self.cursor.execute(
            """SELECT COUNT(*) FROM WriteUserRight
            WHERE user_id={user_id} and task_id={task_id}""".format(
                user_id=self.user_id, task_id=task_id))
        if self.cursor.fetchone()[0] == 1:
            return True
        return False

    def add_writer(self, user_id, task_id):
        """Add write right for user."""
        self.cursor.execute(
            """SELECT COUNT(*) from WriteUserRight
            WHERE user_id={user_id} and task_id={task_id}""".format(
                user_id=user_id, task_id=task_id))
        if self.cursor.fetchone()[0] == 0:
            sql = """INSERT INTO WriteUserRight (user_id, task_id) 
                VALUES ({user_id}, {task_id})""".format(
                user_id=user_id, task_id=task_id)
            self.cursor.execute(sql)
            self.conn.commit()

    def delete_writer(self, user_id, task_id):
        """Delete write right for user."""
        sql = """DELETE from WriteUserRight
            WHERE user_id={user_id} and task_id={task_id}""".format(
            user_id=user_id, task_id=task_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def delete_all_writers(self, task_id):
        """Delete all writers for task."""
        sql = """DELETE from WriteUserRight
            WHERE task_id={task_id}""".format(task_id=task_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def search_writers(self, task_id):
        """Search all writers task."""
        sql = """SELECT * FROM WriteUserRight
            WHERE task_id={task_id}""".format(task_id=task_id)
        users_id = []
        for row in self.cursor.execute(sql):
            users_id.append(row['id'])
        return users_id

    def search_write_tasks(self):
        """Search tasks for user who can write."""
        sql = """SELECT * FROM Task
            INNER JOIN WriteUserRight ON WriteUserRight.task_id=Task.id
            WHERE WriteUserRight.user_id={user_id}
            """.format(user_id=self.user_id)
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(id=row['id'],
                              title=row['title'],
                              note=row['note'],
                              user_id=row['user_id'],
                              tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                              planner_id=row['planner_id'],
                              parent_task_id=row['parent_task_id'],
                              time_start=dateparser.parse(row['time_start']),
                              time_end=dateparser.parse(row['time_end']),
                              time_create=dateparser.parse(row['time_create']),
                              time_update=dateparser.parse(row['time_update']),
                              is_event=bool(row['is_event']),
                              priority=row['priority'],
                              status=row['status']))
        return tasks

    def search_active_tasks(self, user_id):
        sql = """SELECT * FROM Task
               WHERE user_id={user_id} AND 
               (status<>{status1} AND status<>{status2})""".format(
            user_id=user_id, status1=Status.ARCHIVED.value, status2=Status.REPEAT.value
        )
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def search_tasks_by_tag(self, user_id, tag_id):
        sql = """SELECT * FROM Task
               WHERE user_id={user_id} AND tag_id={tag_id} AND
               (status<>{status1} AND status<>{status2})""".format(
            user_id=user_id, tag_id=tag_id,
            status1=Status.ARCHIVED.value, status2=Status.REPEAT.value
        )
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def search_tasks_by_status(self, user_id, status_value):
        sql = """SELECT * FROM Task
               WHERE user_id={user_id} AND status={status_value}""".format(
            user_id=user_id, status_value=status_value
        )
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def search_tasks_not_repeat(self, user_id):
        sql = """SELECT * FROM Task
               WHERE user_id={user_id} AND status!={status_value}""".format(
            user_id=user_id, status_value=Status.REPEAT.value
        )
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def search_tasks_by_priority(self, user_id, priority_value):
        sql = """SELECT * FROM Task
               WHERE user_id={user_id} AND priority={priority_value}AND
               (status<>{status1} AND status<>{status2})""".format(
            user_id=user_id, priority_value=priority_value,
            status1=Status.ARCHIVED.value, status2=Status.REPEAT.value
        )
        tasks = []
        for row in self.cursor.execute(sql):
            tasks.append(Task(
                id=row['id'],
                title=row['title'],
                note=row['note'],
                user_id=row['user_id'],
                tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                planner_id=row['planner_id'],
                parent_task_id=row['parent_task_id'],
                time_start=dateparser.parse(row['time_start']),
                time_end=dateparser.parse(row['time_end']),
                time_create=dateparser.parse(row['time_create']),
                time_update=dateparser.parse(row['time_update']),
                is_event=bool(row['is_event']),
                priority=row['priority'],
                status=row['status']))
        return tasks

    def search_future_tasks(self, user_id):
        sql = """SELECT * FROM Task WHERE user_id={user_id}""".format(user_id=user_id)
        tasks = []
        for row in self.cursor.execute(sql):
            if dateparser.parse(row['time_start']) > datetime.datetime.now():
                tasks.append(Task(
                    id=row['id'],
                    title=row['title'],
                    note=row['note'],
                    user_id=row['user_id'],
                    tag_id=row['tag_id'] if row['tag_id'] != 0 else None,
                    planner_id=row['planner_id'],
                    parent_task_id=row['parent_task_id'],
                    time_start=dateparser.parse(row['time_start']),
                    time_end=dateparser.parse(row['time_end']),
                    time_create=dateparser.parse(row['time_create']),
                    time_update=dateparser.parse(row['time_update']),
                    is_event=bool(row['is_event']),
                    priority=row['priority'],
                    status=row['status']))
        return tasks
