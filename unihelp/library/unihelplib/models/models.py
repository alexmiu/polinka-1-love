"""This module creates previously described entities in the database.

Class of Notification model.
    This class is used to create notifications.
    The notification class has the same state field.
    A state enumeration describes the interaction between
    the notification and the user.

Class of Planner model.
    This class is used to create planners.
    Task have Status REPEAT to work with Planner.

Class of task's tag.
    The tag is an identifier for categorizing,
    describing, searching data and specifying
    an internal structure.

Class of Task model.
    This class is used to create tag.
    The Task class has the same status field.
    A status enumeration describes the interaction between
    the task and the user.
    The Task class has the same priority field.
    A priority enumeration describes importance task.
"""

import datetime
import enum
import sqlite3


class Tag:
    """Tag's are needed to categorizing tasks.

        Class Tag has field:
            id - tag's identification number;
            name - tag's name;
            user_id - user's identification number.
    """
    def __init__(self, id=0, name="", user_id=None):
        self.id = id
        self.name = name
        self.user_id = user_id


class Status(enum.Enum):
    """Enumeration of task state.

    CREATED - was created;
    IN_PROGRESS - in the process of implementation;
    DONE - finished;
    ARCHIVED - in the archive;
    REPEAT - template for cyclic/repeat tasks planner.
    """

    CREATED = 0
    IN_PROGRESS = 1
    DONE = 2
    ARCHIVED = 3
    REPEAT = 4


class Priority(enum.Enum):
    """Enumeration of task priorities.

    MIN - minimal priority;
    MID - midding priority;
    MAX - maximum priority.
    """

    MIN = 0
    MID = 1
    MAX = 2


class Task:
    """The task is a problematic situation with an
        explicit goal, which must be done.

        Class Task has field:
            title - tasks's title;
            note - additional information on the taskж
            id - task's identification number;
            tag - tag's identification number;
            user_id - user's identification number;
            planner_id - planner's identification number;
            parent_task_id - parent task's identification number;
            time_start - time to start task;
            time_end - time to end task;
            time_create - time of creation task;
            time_update - time updated task;
            is_event - is the task an event or simple task;
            priority - execution priority;
            status - describes the current relationship with the user.
    """
    def __init__(self,
                 id=0,
                 title="",
                 note="",
                 time_start=None,
                 time_end=None,
                 planner_id=None,
                 parent_task_id=None,
                 tag_id=None,
                 is_event=False,
                 priority=Priority.MID.value,
                 status=Status.CREATED.value,
                 user_id=None,
                 time_create=datetime.datetime.now(),
                 time_update=datetime.datetime.now()):
        self.id = id
        self.title = title
        self.note = note
        self.time_start = time_start
        self.time_end = time_end
        self.time_create = time_create
        self.time_update = time_update
        self.user_id = user_id
        self.planner_id = planner_id
        self.parent_task_id = parent_task_id
        self.tag_id = tag_id
        self.is_event = is_event
        self.priority = priority
        self.status = status


class Planner:
    """Planners are needed to make periodic tasks with interval.

        Class Notification has field:
            interval - planner time interval to repeat;
            task - task's identification number;
            last_repeat - last time when planner worked;
            user_id - user's identification number;
            id - planner's identification number.
    """
    def __init__(self, id=0, task_id=None, interval=None,
                 user_id=None, last_repeat=datetime.datetime.now()):
        self.id = id
        self.user_id = user_id
        self.task_id = task_id
        self.interval = interval
        self.last_repeat = last_repeat


class State(enum.Enum):
    """Enumeration of notification state.

    CREATED - was created;
    AWAIT - should be shown;
    SHOWN - was shown.
    """

    CREATED = 0
    AWAIT = 1
    SHOWN = 2


class Notification:
    """Notifications are needed to inform the user.

        Class Notification has field:
            title - notification's title;
            task - task that requires notifications;
            work_start_time - time when notification start work;
            user_id - user's identification number;
            state - a state enumeration describes the interaction between
            the notification and the user;
            id - notification's identification number.
    """
    def __init__(self,
                 id=0,
                 title="",
                 task_id=None,
                 work_start_time=datetime.datetime.now(),
                 user_id=None,
                 state=State.CREATED.value):
        self.id = id
        self.title = title
        self.task_id = task_id
        self.user_id = user_id
        self.work_start_time = work_start_time
        self.state = state


class ReadUserRight:
    """Table of users and tasks with read permissions/rights."""
    def __init__(self, task=None, user_id=None):
        self.user_id = user_id
        self.task = task


class WriteUserRight:
    """Table of users and tasks with write permissions/rights."""
    def __init__(self, task=None, user_id=None):
        self.user_id = user_id
        self.task = task


class CreateTables:
    def __init__(self, db_name):
        self.db_name = db_name
        self.conn = sqlite3.connect(self.db_name)
        self.conn.row_factory = sqlite3.Row
        self.cursor = self.conn.cursor()

    def create_table_tag(self):
        self.cursor.execute("""CREATE TABLE Tag
                          (id integer primary key, 
                          name text not null, 
                          user_id integer)
                       """)

    def create_table_task(self):
        self.cursor.execute("""CREATE TABLE Task
                          (id integer primary key, 
                          title text not null,
                          note text default '',
                          time_start text,
                          time_end text,
                          time_create text not null,
                          time_update text not null, 
                          user_id integer,
                          planner_id integer,
                          parent_task_id integer,
                          tag_id integer,
                          is_event integer default 0,
                          priority integer default 1,
                          status integer default 0,
                          foreign key (tag_id) references Tag(id))
                       """)

    def create_table_planner(self):
        self.cursor.execute("""CREATE TABLE Planner
                          (id integer primary key, 
                          task_id integer, 
                          user_id integer,
                          interval integer,
                          last_repeat text not null,
                          foreign key (task_id) references Task(id))
                       """)

    def create_table_notification(self):
        self.cursor.execute("""CREATE TABLE Notification
                          (id integer primary key, 
                          title text not null,
                          task_id integer, 
                          user_id integer,
                          work_start_time text not null,
                          state integer default 0,
                          foreign key (task_id) references Task(id))
                       """)

    def create_table_read_user_right(self):
        self.cursor.execute("""CREATE TABLE ReadUserRight
                          (task_id integer, 
                          user_id integer,
                          foreign key (task_id) references Task(id))
                       """)

    def create_table_write_user_right(self):
        self.cursor.execute("""CREATE TABLE WriteUserRight
                          (task_id integer, 
                          user_id integer,
                          foreign key (task_id) references Task(id))
                       """)
