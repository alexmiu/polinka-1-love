from setuptools import setup, find_packages


setup(
    name='unihelplibrary',
    version='0.8',
    packages=find_packages(),
    test_suite='tests.test')
