import os
import argparse

from unihelplib.logger import setup_logger
from unihelplib.models.models import Priority
import command_line.config as config
from command_line.user import SessionMediator

from command_line.handler import (
    add_user,
    delete_user,
    search_user,
    all_users,
    sign_in,
    logout,
    notification_parser_description,
    planner_parser_description,
    tag_parser_description,
    task_parser_description,
    search_await_notifications)

from unihelplib.storage.mediator import (
    BaseMediator,
    TaskMediator,
    PlannerMediator,
    NotificationMediator)


def user_parser(parser):
    parser.add_parser("add",
                      help="This command append user by login.").add_argument("login")
    parser.add_parser("delete",
                      help="This command delete user by id.").add_argument("id")
    parser.add_parser("sign_in",
                      help="This command authorize user by login.").add_argument("login")
    parser.add_parser("logout",
                      help="This command logout user.")
    parser.add_parser("all",
                      help="This command show all users.")
    parser.add_parser("who",
                      help="This command show current user.")


def user_parser_description(args, session):
    if args.command == "add":
        add_user(args)
    elif args.command == "delete":
        delete_user(args)
    elif args.command == "sign_in":
        sign_in(args, session)
    elif args.command == "logout":
        logout(session)
    elif args.command == "who":
        authorization_check(session)
        search_user(session.who())
    elif args.command == "all":
        all_users()


def tag_parser(parser):
    parser.add_parser(
        "add",
        help="This command add new tag.").add_argument(
        "name",
        help="tag's name")

    parser.add_parser(
        "search",
        help="This command search tag by ID").add_argument(
        "id",
        help="tag's ID")

    edit_parser = parser.add_parser(
        "edit",
        help="THis command edit tag by ID.")

    edit_parser.add_argument(
        "-id",
        "--id",
        help="tag's ID",
        required=True)
    edit_parser.add_argument(
        "-n",
        "--name",
        help="tag's name",
        required=True)

    parser.add_parser(
        "delete", help="This command delete tag by ID.").add_argument(
        "id", help="tag's ID")

    parser.add_parser("all", help="This command show all tags.")


def planner_parser(parser):
    edit_parser = parser.add_parser(
        "edit",
        help="This command edit planner.")
    edit_parser.add_argument(
        "id",
        help="planner ID")
    edit_parser.add_argument(
        "-r",
        "--repeat",
        help="This command edit cycle task with interval to repeat."
             "You can use: -r 'every 2 days'")
    edit_parser.add_argument(
        "-tw",
        "--time_work",
        help="Sets date for work."
             "You can use: -tw 'in 5 week'")

    parser.add_parser(
        "all",
        help="This command print all planners.")
    parser.add_parser(
        "delete",
        help="This command delete planner."
    ).add_argument("id", help="planner's id")


def notification_parser(parser):
    add_parser = parser.add_parser(
        "add",
        help="This command add notification.",
        usage="unihelp notification add ")
    required = add_parser.add_argument_group('required arguments')
    optional = add_parser.add_argument_group('optional arguments')
    required.add_argument(
        "-ti",
        "--title",
        help="notification's title",
        required=True)
    required.add_argument(
        '-wst',
        '--work_start_time',
        help="notification's time. Example:'14:30'",
        required=True)
    optional.add_argument(
        "-t",
        "--task",
        help="task's ID",
        default=None)

    edit_parser = parser.add_parser(
        "edit",
        help="This command edit notification.")
    required_edit = edit_parser.add_argument_group('required arguments')
    optional_edit = edit_parser.add_argument_group('optional arguments')
    required_edit.add_argument(
        "-i",
        "--id",
        help="notification's ID",
        required=True)
    optional_edit.add_argument(
        "-t",
        "--title",
        help="notification's title")
    optional_edit.add_argument(
        "-n",
        "--note",
        help="notification's note")
    optional_edit.add_argument(
        "-wst",
        "--work_start_time",
        help="notification's work start time")

    search_parser = parser.add_parser(
        "search",
        help="Show notifications")
    subparser = search_parser.add_subparsers(
        dest="search_command",
        title="Show notifications",
        description="Commands to search notification.",
        metavar='')
    search_notification_parser(subparser)

    parser.add_parser(
        "delete",
        help="This command delete notification by ID.").add_argument(
        "id",
        help="notification's ID")


def search_notification_parser(parser):
    parser.add_parser(
        "id",
        help="This command search notification by id.",
        usage="unihelp notification search id ").add_argument(
        "id",
        help="notification's ID")

    parser.add_parser(
        "created",
        help="This command search user's created notifications.")
    parser.add_parser(
        "shown",
        help="This command search user's shown notifications.")
    parser.add_parser(
        "all",
        help="This command search all user's notifications.")


def task_parser(parser):
    add_parser = parser.add_parser(
        "add", help="This command add new task.", usage="unihelp task add ")
    required = add_parser.add_argument_group('required arguments')
    optional = add_parser.add_argument_group('optional arguments')
    required.add_argument(
        "-t", "--title", help="task's title", required=True)
    optional.add_argument("-n", "--note", help="task's note")
    optional.add_argument("-ts", "--time_start", help="task's time start (28 August 2018 at 09:00)")
    optional.add_argument("-te", "--time_end", help="task's time end (29 August 2018 at 09:00)")
    optional.add_argument(
        "-ie", "--is_event",
        help="is task event",
        choices=["yes", "no"])
    optional.add_argument("-tag", "--tag", help="task's tag_id")
    optional.add_argument(
        "-p", "--priority",
        choices=Priority.__members__,
        help="task's priority")
    optional.add_argument(
        "-r", "--repeat",
        help="This command make cycle task with interval."
        "You can use: --repeat every '1 hour'. "
             "This field sets the frequency of the task creation.")
    optional.add_argument(
        "-sa", "--start_repeat_at",
        help="Start date time for cycle task."
             "You can use: --sa in '14:30'. "
             "This field specifies the start time for the task repeat.")

    edit_parser = parser.add_parser('edit', help="This command edit task.")
    required_edit = edit_parser.add_argument_group('required arguments')
    optional_edit = edit_parser.add_argument_group('optional arguments')
    required_edit.add_argument('-id', '--id', help="task's ID", required=True)
    optional_edit.add_argument('-t', '--title', help="task's title")
    optional_edit.add_argument('-n', '--note', help="task's note")
    optional_edit.add_argument('-ts', '--time_start', help="task's time start")
    optional_edit.add_argument('-te', '--time_end', help="task's time end")
    optional_edit.add_argument(
        '-ie', '--is_event',
        help="is task event",
        choices=['yes', 'no'])
    optional_edit.add_argument('-tag', '--tag', help="task's tag ID")
    optional_edit.add_argument(
        '-p', '--priority',
        choices=Priority.__members__,
        help="task's priority")

    search_parser = parser.add_parser('search', help="Search tasks.")
    search_subparser = search_parser.add_subparsers(
        dest='search_command',
        title='Search tasks.',
        description='This command search task.',
        metavar='')
    create_search_task_parser(search_subparser)

    parser.add_parser(
        'delete',
        help="This command delete task by ID.").add_argument(
        'id',
        help="task's ID")

    status_parser = parser.add_parser(
        'set_status', help='Set status')
    status_subparser = status_parser.add_subparsers(
        dest='set_status_command',
        title='Set status',
        description='This command set status to task.',
        metavar='')
    status_subparser.add_parser(
        'in_progress',
        help="This command set status task IN_PROGRESS.").add_argument(
        'id',
        help="task's ID")
    status_subparser.add_parser(
        'done',
        help="This command set status task DONE.").add_argument(
        'id',
        help="task's ID")
    status_subparser.add_parser(
        'archived',
        help="This command set status task ARCHIVED.").add_argument(
        'id',
        help="task's ID")

    subtask_parser = parser.add_parser(
        'add_subtask',
        help="This command add subtask to parent task.")
    required = subtask_parser.add_argument_group('required arguments')
    optional = subtask_parser.add_argument_group('optional arguments')
    required.add_argument(
        '-pt', '--parent_task', help="parent task's ID", required=True)
    required.add_argument(
        '-t', '--title', help="task's title", required=True)
    optional.add_argument(
        '-n', '--note', help="task's note")
    optional.add_argument(
        '-ts', '--time_start', help="task's time start")
    optional.add_argument(
        '-te', '--time_end', help="task's time end")
    optional.add_argument(
        '-ie', '--is_event', help="is task event", choices=['yes', 'no'])
    optional.add_argument(
        '-tag', '--tag', help="task's tag ID")
    optional.add_argument(
        '-p',
        '--priority',
        choices=Priority.__members__,
        help="task's priority")

    right_parser = parser.add_parser(
        'right',
        help="Work with right")
    right_subparser = right_parser.add_subparsers(
        dest='right_command',
        title='Work with right',
        description='This command work with right to tasks.',
        metavar='')
    create_right_parser(right_subparser)


def create_right_parser(parser):
    add_parser = parser.add_parser(
        'add',
        help='Adds right',
        usage='unihelp task right add ')
    add_subparser = add_parser.add_subparsers(
        dest='right_add_command',
        title='Adds right',
        description='This command add right for user.',
        metavar='')

    read_parser = add_subparser.add_parser(
        'read', help='This command add read rights.')
    read_parser.add_argument(
        '-t', '--task', help="task's ID", required=True)
    read_parser.add_argument(
        '-u', '--user', help="user's ID", required=True)

    write_parser = add_subparser.add_parser(
        'write', help='This command add write rights.')
    write_parser.add_argument(
        '-t', '--task', help="task's ID", required=True)
    write_parser.add_argument(
        '-u', '--user', help="user's ID", required=True)

    delete_parser = parser.add_parser(
        'delete',
        help='This command delete right for task.',
        usage='unihelp task right delete ')
    delete_subparser = delete_parser.add_subparsers(
        dest='right_delete_command',
        title='This command delete right for task.',
        description='Command delete right for task.',
        metavar='')

    delete_read = delete_subparser.add_parser(
        'read', help='This command delete user right for read task.')
    delete_read.add_argument(
        '-t', '--task', help="task's ID", required=True)
    delete_read.add_argument(
        '-u', '--user', help="user's ID", required=True)

    delete_write = delete_subparser.add_parser(
        'write', help='This command delete user right for write task.')
    delete_write.add_argument(
        '-t', '--task', help="task's ID", required=True)
    delete_write.add_argument(
        '-u', '--user', help="user's ID", required=True)


def create_search_task_parser(parser):
    parser.add_parser(
        'id',
        help='This command search task by id.',
        usage='unihelp task search id ').add_argument(
        'id',
        help="task's ID")

    parser.add_parser('all', help="This command search all user's tasks.")

    search_subtask_parser = parser.add_parser(
        'subtask', help="This command search subtasks by parent task's ID.")
    search_subtask_parser.add_argument('pid', help="parent task's ID")

    parser.add_parser(
        'parent',
        help="This command search parent by subtask task's ID.").add_argument(
        'id',
        help="subtask task's ID")

    parser.add_parser('in_progress', help="This command search user's in progress tasks.")
    parser.add_parser('done', help="This command search user's done tasks.")
    parser.add_parser('archived', help="This command search user's archived tasks.")

    parser.add_parser(
        'can_read',
        help="This command search tasks that user can read.")

    parser.add_parser(
        'can_write',
        help="This command search tasks that user can read and write.")


def authorization_check(session):
    if session.who() is None:
        print("Ops. User not sign in.")
        quit()


def parser_object(args, session):
    if args.object == 'user':
        user_parser_description(args, session)
    elif args.object == 'tag':
        authorization_check(session)
        tag_parser_description(args, session.who())
    elif args.object == 'task':
        authorization_check(session)
        task_parser_description(args, session.who())
    elif args.object == 'notification':
        authorization_check(session)
        notification_parser_description(args, session.who())
    elif args.object == 'planner':
        authorization_check(session)
        planner_parser_description(args, session.who())


def setup_parser():
    parser = argparse.ArgumentParser(
        description="This application let work with the tracker unihelp.",
                    usage='unihelp <object> [<args>]')
    subparsers = parser.add_subparsers(
        dest='object',
        title="Commands",
        description="Choose object for begin work",
        metavar='')

    user = subparsers.add_parser(
        'user',
        help="Commands with user",
        usage='unihelp user ')
    user_pars = user.add_subparsers(
        dest='command',
        title="Commands with user",
        description="Commands to work with users.",
        metavar='')

    user_parser(user_pars)

    tag = subparsers.add_parser(
        'tag',
        help="Commands with tag",
        usage='unihelp tag ')
    tag_pars = tag.add_subparsers(
        dest='command',
        title="Commands with tag",
        description="Commands to work with tags.",
        metavar='')

    tag_parser(tag_pars)

    task = subparsers.add_parser(
        'task',
        help="Commands with task",
        usage='unihelp task ')
    task_pars = task.add_subparsers(
        dest='command',
        title="Commands with task",
        description="Commands to work with tasks.",
        metavar='')

    task_parser(task_pars)

    planner = subparsers.add_parser(
        'planner',
        help="Commands with planner",
        usage='unihelp planner ')
    planner_pars = planner.add_subparsers(
        dest='command',
        title="Commands with planner",
        description="Commands to work with planner.",
        metavar='')

    planner_parser(planner_pars)

    notification = subparsers.add_parser(
        'notification',
        help="Commands with notification",
        usage='unihelp notification ')
    notification_pars = notification.add_subparsers(
        dest='command',
        title="Commands with notification",
        description="Commands to work with notifications.",
        metavar='')

    notification_parser(notification_pars)

    return parser


def create_directory_app():
    if not os.path.exists(config.DIRECTORY_APP):
        os.makedirs(config.DIRECTORY_APP)

    setup_logger(
        enabled=config.LOGGING_ENABLED,
        is_stdout=config.IS_STDOUT,
        log_level=config.LOG_LEVEL,
        handler_path=config.LOG_FILE,
        log_format=config.LOG_FORMAT)


def main():
    create_directory_app()
    session = SessionMediator()
    current_user = session.who()

    if current_user is not None:
        NotificationMediator(
            current_user.id, config.DEFAULT_DATABASE).progress()

        PlannerMediator(
            current_user.id, config.DEFAULT_DATABASE).progress(
                TaskMediator(current_user.id, config.DEFAULT_DATABASE))

        search_await_notifications(current_user)

    parser = setup_parser()
    args = parser.parse_args()
    parser_object(args, session)


if __name__ == "__main__":
    main()
