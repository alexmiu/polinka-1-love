from setuptools import setup, find_packages


setup(
    name="unihelp",
    version="1.0",
    install_requires=["peewee", "dateparser"],
    packages=find_packages(),
    entry_points={
        'console_scripts':
            [
                'unihelp = command_line.main:main'
            ]
    }
)
