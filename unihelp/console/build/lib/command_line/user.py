"""This module includes a class User, class Session, class SessionMediator, class UserMediator."""

import command_line.config as config
import sqlite3


class User:
    """This class describes the user who uses the tracker.
    Is needed to implement the table in the database.

    Class has fields:
        id - identification number in the database;
        login - individual name.
    """
    def __init__(self,
                 id=0,
                 login=""):
        self.id = id
        self.login = login


class Session:
    """This class describes who work with unihelp.

        Class has fields:
            name - identification name in the database;
            is_login - is user sign_in;
            user_id - user who sign_in unihelp.
    """
    def __init__(self,
                 name="",
                 is_login=False,
                 user_id=0):
        self.name = name
        self.is_login = is_login
        self.user_id = user_id


class CreateTables:
    def __init__(self, db_name=config.DEFAULT_DATABASE):
        self.db_name = db_name
        self.conn = sqlite3.connect(self.db_name)
        self.conn.row_factory = sqlite3.Row
        self.cursor = self.conn.cursor()

    def create_table_user(self):
        self.cursor.execute("""CREATE TABLE User
                          (id integer primary key, 
                          login text not null)""")

    def create_table_session(self):
        self.cursor.execute("""CREATE TABLE Session
                          (name text not null, 
                          is_login integer,
                          user_id integer)""")


class BaseMediator:
    def __init__(self, db_name=config.DEFAULT_DATABASE):
        self.db_name = db_name

        tables = CreateTables(db_name)
        self.cursor = tables.cursor
        self.conn = tables.conn
        self.conn.row_factory = sqlite3.Row

        sql = """SELECT COUNT (*)
                    FROM sqlite_master WHERE type='table'
                    AND name='{table_name}'""".format(table_name="User")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_user()

        sql = """SELECT COUNT (*)
                            FROM sqlite_master WHERE type='table'
                            AND name='{table_name}'""".format(table_name="Session")
        self.cursor.execute(sql)
        if self.cursor.fetchone()[0] == 0:
            tables.create_table_session()

    def counter_table(self, Table):
        self.cursor.execute(
            """SELECT COUNT(*) FROM {Table}""".format(Table=Table))
        result = self.cursor.fetchone()[0]
        return result if result is not None else 0


class UserMediator(BaseMediator):
    """Class mediator for working with information in
     the database."""
    def create(self, user):
        """Creates user in database."""
        if self.search_by_login(user.login) is None:
            sql = """INSERT INTO User (login)
                             VALUES ('{login}')""".format(login=user.login)
            self.cursor.execute(sql)
            self.conn.commit()

            user.id = self.cursor.lastrowid
            return user

    def delete(self, user_id):
        """Deletes user in database."""
        sql = "DELETE from User WHERE id={user_id}".format(user_id=user_id)
        self.cursor.execute(sql)
        self.conn.commit()

    def search(self, user_id):
        """Searches user by id in database."""
        sql = """SELECT * FROM User
            WHERE id={user_id}""".format(user_id=user_id)
        self.cursor.execute(sql)
        result = self.cursor.fetchone()
        if not result:
            return None
        return User(id=result['id'],
                   login=result['login'])

    def search_by_login(self, login):
        """Searches user by login in database."""
        sql = """SELECT * FROM User
                    WHERE login='{login}'""".format(login=login)
        self.cursor.execute(sql)
        result = self.cursor.fetchone()
        if not result:
            return None
        return User(id=result['id'],
                    login=result['login'])

    def search_all(self):
        """Searches all users in database."""
        sql = """SELECT * FROM User"""
        users = []
        for row in self.cursor.execute(sql):
            users.append(User(id=row['id'],
                            login=row['login']))
        return users


class SessionMediator(BaseMediator):
    """This class implements the logic of sign in and out of the system."""
    def create(self, session):
        sql = """INSERT INTO Session (name, is_login, user_id)
                 VALUES ('{name}',
                        {is_login},
                        {user_id})""".format(
            name="current",
            is_login=int(session.is_login),
            user_id=session.user_id)
        self.cursor.execute(sql)
        self.conn.commit()
        return session

    def delete(self):
        """Deletes session in database."""
        sql = "DELETE from Session WHERE name='{name}'".format(name="current")
        self.cursor.execute(sql)
        self.conn.commit()

    def update(self, session):
        sql = """UPDATE Session
            SET is_login={is_login}, user_id={user_id}
            WHERE name='{name}'""".format(
            name="current",
            is_login=int(session.is_login),
            user_id=session.user_id)
        self.cursor.execute(sql)
        self.conn.commit()
        return session

    def search(self):
        sql = """SELECT * FROM Session
                    WHERE name='{name}'""".format(name="current")
        self.cursor.execute(sql)
        result = self.cursor.fetchone()
        if not result:
            return None
        return Session(name=result['name'],
                       is_login=bool(result['is_login']),
                       user_id=result['user_id'])

    def sign_in(self, login):
        """Searches for a user by login and writes
        it to database.

        All actions of the unihelp will be with the
        id parameter of this user.
        """
        user = UserMediator().search_by_login(login)
        if user is not None:
            if BaseMediator().counter_table("Session") == 1:
                self.update(Session(
                    is_login=True,
                    user_id=user.id))
            else:
                self.create(Session(
                    is_login=True,
                    user_id=user.id))
        else:
            print("Oops. User not found.")
        return user

    def logout(self):
        """Deletes the user from session in database."""
        if BaseMediator().counter_table("Session") == 1:
            self.update(Session(
                is_login=False,
                user_id=0))
        else:
            print("Error. Nobody login.")

    def who(self):
        """Search who sign in unihelp."""
        if self.search() is not None:
            return UserMediator().search(
                user_id=self.search().user_id)
        else:
            return None
