"""This module includes a class User, class Session, class UserMediator."""

from peewee import (
    Model,
    CharField,
    IntegerField,
    BooleanField,
    DoesNotExist,
    SqliteDatabase,
    PrimaryKeyField)

import command_line.config as config

database = SqliteDatabase(config.DEFAULT_DATABASE)


class User(Model):
    """This class describes the user who uses the tracker.
    Is needed to implement the table in the database.

    Class has fields:
        id - identification number in the database;
        login - individual name.
    """
    id = PrimaryKeyField(null=False)
    login = CharField()

    class Meta:
        database = database


class Session(Model):
    """This class describes who work with unihelp.

        Class has fields:
            name - identification name in the database;
            is_login - is user sign_in;
            user_id - user who sign_in unihelp.
    """
    name = CharField()
    is_login = BooleanField(default=False)
    user_id = IntegerField(default=None)

    class Meta:
        database = database


class DataBaseInit:
    """Initializes the database."""
    def __init__(self):
        User.create_table(True)
        Session.create_table(True)


class UserMediator(DataBaseInit):
    """Class mediator for working with information in
     the database."""
    def create(self, user):
        """Creates user in database."""
        if self.search_by_login(user.login) is None:
            return User.create(
                id=user.id,
                login=user.login)

    @staticmethod
    def delete(user_id):
        """Deletes user in database."""
        User.delete().where(User.id == user_id).execute()

    @staticmethod
    def search(user_id):
        """Searches user by id in database."""
        try:
            return User.get(User.id == user_id)
        except DoesNotExist:
            return None

    @staticmethod
    def search_by_login(login):
        """Searches user by login in database."""
        try:
            return User.get(User.login == login)
        except DoesNotExist:
            return None

    @staticmethod
    def search_all():
        """Searches all users in database."""
        return list(
            User.select(User.id, User.login))


class SessionMediator(DataBaseInit):
    """This class implements the logic of sign in and out of the system."""
    def create(self, session):
        return Session.create(
            name="current",
            is_login=session.is_login,
            user_id=session.user_id)

    def delete(self):
        """Deletes session in database."""
        Session.where(Session.name == "current").delete().execute()

    def update(self, session):
        return Session.update(
            is_login=session.is_login,
            user_id=session.user_id).where(
            Session.name == "current").execute()

    def search(self):
        try:
            return Session.get(Session.name == "current")
        except DoesNotExist:
            return None

    def sign_in(self, login):
        """Searches for a user by login and writes
        it to database.

        All actions of the unihelp will be with the
        id parameter of this user.
        """
        user = UserMediator().search_by_login(login)
        if user is not None:
            if Session.select().count() == 1:
                self.update(Session(
                    is_login=True,
                    user_id=user.id))
            else:
                self.create(Session(
                    is_login=True,
                    user_id=user.id))
        else:
            print("Oops. User not found.")
        return user

    def logout(self):
        """Deletes the user from session in database."""
        if Session.select().count() == 1:
            self.update(Session(
                is_login=False,
                user_id=None))
        else:
            print("Error. Nobody login.")

    def who(self):
        """Search who sign in unihelp."""
        if self.search() is not None:
            user = UserMediator().search(
                user_id=self.search().user_id)
            print("login:{}".format(user.login))
