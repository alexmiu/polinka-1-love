import sys
import datetime
import dateparser

from unihelplib.models.models import (
    State,
    Status,
    Priority,
    Tag,
    Task,
    Planner,
    Notification)
from unihelplib.storage.mediator import (
    TagMediator,
    TaskMediator,
    PlannerMediator,
    NotificationMediator)
import unihelplib.actions as actions
from unihelplib.exceptions.exceptions import AccessRights
import command_line.config as config
from command_line.user import (
    User,
    UserMediator)


def add_user(args):
    user = User(login=args.login)
    if UserMediator().create(user):
        print("User {} added.".format(args.login))
    else:
        print("Error. User {} already exist."
              "Please chose some different login.".format(args.login), file=sys.stdout)


def delete_user(args):
    UserMediator().delete(args.id)
    print("User deleted.")


def sign_in(args, session):
    if session.sign_in(args.login) is not None:
        print("The user {} has successfully"
              " logged in system".format(args.login))
    else:
        print("Error. User {} doesn't"
              " exist!".format(args.login), file=sys.stdout)


def logout(session):
    session.logout()
    print("User left.")


def search_user(user):
    print("Login {} == ID {}".format(user.login, user.id))


def all_users():
    print("Users:")
    users = UserMediator().search_all()
    for user in users:
        print("Login {} == ID {}".format(user.login, user.id))


def tag_parser_description(args, user):
    if args.command == "add":
        add_tag(args, user)
    elif args.command == "search":
        search_tag(args, user)
    elif args.command == "delete":
        delete_tag(args, user)
    elif args.command == "edit":
        edit_tag(args, user)
    elif args.command == "all":
        search_tags(user)


def add_tag(args, user):
    tag = Tag(name=args.name)
    actions.add_tag(
        TagMediator(user.id, config.DEFAULT_DATABASE), tag)
    print("Tag {} added.".format(args.name))


def edit_tag(args, user):
    tag = actions.search_tag(
        TagMediator(user.id, config.DEFAULT_DATABASE), args.id)
    if tag is None:
        print("Oops. This tag doesn't exist.")
        quit()
    tag.name = args.name
    actions.update_tag(
        TagMediator(user.id, config.DEFAULT_DATABASE), tag)
    print("Tag {} == ID {} updated.".format(tag.name, tag.id))


def delete_tag(args, user):
    actions.delete_tag(
        TagMediator(user.id, config.DEFAULT_DATABASE), args.id)
    print("Tag deleted.")


def search_tag(args, user):
    tag = actions.search_tag(
        TagMediator(user.id, config.DEFAULT_DATABASE), args.id)
    print("Tag {} == ID {}".format(tag.name, tag.id))


def search_tags(user):
    print("Tags:")
    tags = actions.user_tags(
        TagMediator(user.id, config.DEFAULT_DATABASE))
    for tag in tags:
        print("{} == ID {}".format(tag.name, tag.id))


def planner_parser_description(args, user):
    if args.command == "edit":
        edit_planner(args, user)
    elif args.command == "all":
        search_planners(user)
    elif args.command == "delete":
        delete_planner(args, user)


def edit_planner(args, user):
    planner = actions.search_planner(
        PlannerMediator(user.id, config.DEFAULT_DATABASE), args.id)

    if planner is None:
        print("Error. Planner with this ID doesn't exist.",
              file=sys.stdout)
        quit()

    if args.repeat is not None:
        convert_time = dateparser.parse(args.repeat)
        if convert_time is None:
            print("Error. Please check the repeat time.", file=sys.stdout)
            quit()

        interval = (datetime.datetime.now() - convert_time).total_seconds()
        if interval < 60:
            print("Error. Interval is too short!", file=sys.stdout)
            quit()

        planner.interval = interval
        if args.time_work is not None:
            time_work = dateparser.parse(args.time_work)
            if time_work is not None:
                planner.last_repeat = time_work - datetime.datetime.now() - convert_time
                actions.update_planner(PlannerMediator(user.id, config.DEFAULT_DATABASE), planner)
                print("Updated planner.")
            else:
                print("Error. Please check time to repeat.", file=sys.stdout)


def delete_planner(args, user):
    actions.delete_planner(
        PlannerMediator(user.id, config.DEFAULT_DATABASE),
        args.id)
    print("Planner deleted.")


def search_planners(user):
    planners = actions.search_all_planners(
        PlannerMediator(user.id, config.DEFAULT_DATABASE))
    print_planners(planners)


def print_planners(planners):
    if planners is not None:
        for planner in planners:
            print_planner(planner)


def print_planner(planner):
    print(
        "ID:{} | task ID:{} | interval: {} | last work: {}".format(
            planner.id,
            planner.task_id,
            datetime.timedelta(
                seconds=planner.interval),
            planner.last_repeat))


def notification_parser_description(args, user):
    if args.command == "add":
        add_notification(args, user)
    elif args.command == "search":
        if args.search_command == "id":
            search_notification(args, user)
        elif args.search_command == "all":
            search_all_notifications(user)
        elif args.search_command == "created":
            search_notification_by_state(
                user=user, state=State.CREATED)
        elif args.search_command == "shown":
            search_notification_by_state(
                user=user, state=State.SHOWN)
    elif args.command == "edit":
        edit_notification(args, user)
    elif args.command == "delete":
        delete_notification(args, user)


def add_notification(args, user):
    if args.task is not None:
        task = actions.search_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE), args.task)
        if task is None:
            print("Error. Task doesn't exist.", file=sys.stdout)
            quit()
    else:
        task = None

    work_start_time = dateparser.parse(args.work_start_time)
    notification = Notification(
        task=task,
        title=args.title,
        work_start_time=work_start_time)

    actions.add_notification(
        NotificationMediator(user.id, config.DEFAULT_DATABASE),
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        notification)
    print("Notification added.")


def delete_notification(args, user):
    actions.delete_notification(
        NotificationMediator(user.id, config.DEFAULT_DATABASE), args.id)
    print("Notification deleted.")


def search_all_notifications(user):
    notifications = actions.search_all_notification(
        NotificationMediator(user.id, config.DEFAULT_DATABASE))
    print_notifications(notifications)


def search_notification(args, user):
    notification = actions.search_notification(
        NotificationMediator(user.id, config.DEFAULT_DATABASE),
        args.id)
    print_notification(notification)


def search_notification_by_state(user, state):
    notifications = actions.search_notification_by_state(
        NotificationMediator(user.id, config.DEFAULT_DATABASE), state)
    print_notifications(notifications)


def print_notification(notification):
    print(
        "ID:{} | title: {} |"
        " time of notifications: {} | state: {}".format(
            notification.id,
            notification.title,
            notification.work_start_time,
            State(notification.state).name))
    if notification.task is not None:
        print("task ID:{}".format(notification.task.id))


def print_notifications(notifications):
    if notifications is not None:
        for notification in notifications:
            print_notification(notification)


def edit_notification(args, user):
    notification = actions.search_notification(
        NotificationMediator(user.id, config.DEFAULT_DATABASE), args.id)
    if notification is None:
        print("Error. Notification with this ID doesn't exist!",
              file=sys.stdout)
    if args.title is not None:
        notification.title = args.title
    if args.work_start_time is not None:
        convert_time = dateparser.parse(args.work_start_time)
        if convert_time is not None:
            notification.work_start_time = (
                    datetime.datetime.now() -
                    convert_time).total_seconds()
    actions.update_notification(
        NotificationMediator(user.id, config.DEFAULT_DATABASE),
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        notification)
    print("Notification updated.")


def task_parser_description(args, user):
    if args.command == 'add':
        add_task(args, user)
    elif args.command == 'edit':
        edit_task(args, user)
    elif args.command == 'search':
        if args.search_command == 'id':
            search_task(args, user)
        elif args.search_command == 'all':
            search_all_tasks(user)
        elif args.search_command == 'subtask':
            search_subtasks(args, user)
        elif args.search_command == 'parent':
            search_parent_task(args, user)
        elif args.search_command == 'in_progress':
            search_in_progress_tasks(user)
        elif args.search_command == 'done':
            search_done_tasks(user)
        elif args.search_command == 'archived':
            search_archived_tasks(user)
        elif args.search_command == 'can_read':
            search_can_read_tasks(user)
        elif args.search_command == 'can_write':
            search_can_write_tasks(user)
    elif args.command == 'set_status':
        if args.set_status_command == 'in_progress':
            set_task_as_in_progress(args, user)
        elif args.set_status_command == 'done':
            set_task_as_done(args, user)
        elif args.set_status_command == 'archived':
            set_task_as_archived(args, user)
    elif args.command == 'delete':
        delete_task(args, user)
    elif args.command == 'add_subtask':
        create_subtask(args, user)
    elif args.command == 'right':
        if args.right_command == 'add':
            if args.right_add_command == 'read':
                add_reader(args, user)
            elif args.right_add_command == 'write':
                add_writer(args, user)
        elif args.right_command == 'delete':
            if args.right_delete_command == 'read':
                delete_reader(args, user)
            elif args.right_delete_command == 'write':
                delete_writer(args, user)


def search_task(args, user):
    task = actions.search_task(
        TaskMediator(user.id, config.DEFAULT_DATABASE), args.id)
    print_task(task, user)


def delete_task(args, user):
    try:
        actions.delete_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE), args.id)
        print('Task with ID {} deleted'.format(args.id))
    except AccessRights:
        print("User hasn't rights for this action", file=sys.stdout)


def set_task_as_in_progress(args, user):
    actions.set_task_status(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        args.id,
        Status.IN_PROGRESS.value)


def set_task_as_done(args, user):
    actions.set_task_status(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        args.id,
        Status.DONE.value)


def set_task_as_archived(args, user):
    actions.set_task_status(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        args.id,
        Status.ARCHIVED.value)


def search_subtasks(args, user):
    print('Subtasks:')
    if actions.search_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE),
            args.pid) is not None:
        tasks = actions.search_subtasks(
            TaskMediator(user.id, config.DEFAULT_DATABASE),
            args.pid, True)
        print_tasks(tasks, user)
    else:
        print("Task doesn't exist.", file=sys.stdout)


def search_parent_task(args, user):
    print('Parent task:')
    if actions.search_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE),
            args.id) is not None:
        task = actions.search_parent_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE), args.id)
        print_task(task, user)
    else:
        print("Task doesn't exist.", file=sys.stdout)


def add_reader(args, user):
    actions.add_reader(
        task_mediator=TaskMediator(
            user.id, config.DEFAULT_DATABASE),
        user_id=args.user,
        task_id=args.task)


def add_writer(args, user):
    actions.add_writer(
        task_mediator=TaskMediator(
            user.id, config.DEFAULT_DATABASE),
        user_id=args.user,
        task_id=args.task)


def delete_reader(args, user):
    actions.delete_reader(
        task_mediator=TaskMediator(
            user.id, config.DEFAULT_DATABASE),
        user_id=args.user,
        task_id=args.task)


def delete_writer(args, user):
    actions.delete_writer(
        task_mediator=TaskMediator(
            user.id, config.DEFAULT_DATABASE),
        user_id=args.user,
        task_id=args.task)


def search_all_tasks(user):
    print('Tasks:')
    tasks = actions.search_user_tasks(
        TaskMediator(user.id, config.DEFAULT_DATABASE))
    print_tasks(tasks, user)


def search_connected_tasks(user):
    print('Subtasks:')
    tasks = actions.search_user_tasks(
        TaskMediator(user.id, config.DEFAULT_DATABASE))
    print_tasks(tasks, user)


def search_can_read_tasks(user):
    print('Can read tasks:')
    tasks = actions.user_can_read_task(
        TaskMediator(user.id, config.DEFAULT_DATABASE))
    print_tasks(tasks, user)


def search_can_write_tasks(user):
    print('Can write tasks:')
    tasks = actions.user_can_write_task(
        TaskMediator(user.id, config.DEFAULT_DATABASE))
    print_tasks(tasks, user)


def search_in_progress_tasks(user):
    print('IN_PROGRESS:')
    tasks = actions.tasks_by_status(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        Status.IN_PROGRESS.value)
    print_tasks(tasks, user)


def search_done_tasks(user):
    print('DONE:')
    tasks = actions.tasks_by_status(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        Status.DONE.value)
    print_tasks(tasks, user)


def search_archived_tasks(user):
    print('ARCHIVED:')
    tasks = actions.tasks_by_status(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        Status.ARCHIVED.value)
    print_tasks(tasks, user)


def print_task(task, user):
    if task is not None:
        result = "ID: {} | title: {}".format(task.id, task.title)
        if task.parent_task_id is not None:
            result += " | parent task's id: {}".format(task.parent_task_id)
        if task.note is not "":
            result += " | note: {}".format(task.note)
        if task.time_start is not None:
            result += " | start time: {}".format(task.time_start)
        if task.time_end is not None:
            result += " | end time: {}".format(task.time_end)
        if task.tag_id is None:
            result += " | tag: None"
        else:
            result += " | tag: {}".format(
                actions.search_tag(
                    TagMediator(user.id, config.DEFAULT_DATABASE),
                    task.tag_id).name)
        result += " | is event: {} | priority: {} | status: {}".format(
            task.is_event, Priority(
                task.priority).name, Status(
                task.status).name)
        result += " | created at: {} | updated_at: {}".format(
            task.time_create, task.time_update)
        print(result)


def print_tasks(tasks, user):
    if tasks is not None:
        for task in tasks:
            print_task(task, user)


def create_subtask(args, user):
    task = Task(title=args.title)
    if args.note is not None:
        task.note = args.note
    if args.time_start is not None:
        task.time_start = dateparser.parse(args.time_start)
    if args.time_end is not None:
        task.time_end = dateparser.parse(args.time_end)
        check_time(task.time_start, task.time_end)
    if args.is_event is not None:
        task.is_event = args.is_event == 'yes'
    if args.tag is not None:
        task.tag = args.tag
    if args.priority is not None:
        task.priority = Priority[args.priority.upper()].value
    actions.create_subtask(
        TaskMediator(user.id, config.DEFAULT_DATABASE),
        args.parent_task,
        task)
    print("Subtask created.")


def check_time(start, end):
    if start is not None and end is not None:
        if start > end:
            print("Error. Start time greater than end time.", file=sys.stdout)
            quit()


def edit_task(args, user):
    task = actions.search_task(
        TaskMediator(user.id, config.DEFAULT_DATABASE), args.id)
    if task is not None:
        if args.title is not None:
            task.title = args.title
        if args.note is not None:
            task.note = args.note
        if args.time_start is not None:
            task.time_start = dateparser.parse(args.time_start)
        if args.time_end is not None:
            task.time_end = dateparser.parse(args.time_end)
            check_time(task.time_start, task.time_end)
        if args.is_event is not None:
            task.is_event = args.is_event == 'yes'
        if args.tag is not None:
            task.tag = args.tag
        if args.priority is not None:
            task.priority = Priority[args.priority.upper()].value
        actions.update_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE), task)
        print("Task updated.")
    else:
        print("Error. Task doesn't exist!", file=sys.stdout)


def add_task(args, user):
    task = Task(title=args.title)
    if args.note is not None:
        task.note = args.note
    if args.time_start is not None:
        task.time_start = dateparser.parse(args.time_start)
    if args.time_end is not None:
        task.time_end = dateparser.parse(args.time_end)
        check_time(task.time_start, task.time_end)
    if args.is_event is not None:
        task.is_event = args.is_event == 'yes'
    if args.tag is not None:
        task.tag = args.tag
    if args.priority is not None:
        task.priority = Priority[args.priority.upper()].value
    if args.repeat is not None:
        convert_time = dateparser.parse(args.repeat)
        if convert_time is None:
            print("Error. Repeat time is incorrect.", file=sys.stdout)
            quit()

        interval = (datetime.datetime.now() - convert_time).total_seconds()
        if interval < 60:
            print("Error. Task's interval is incorrect", file=sys.stdout)
            quit()

        if args.start_repeat_at is not None:
            convert_time = dateparser.parse(args.start_repeat_at)
            if convert_time is None:
                print("Error. Work time incorrect.", file=sys.stdout)
                quit()
        delta = datetime.datetime.now() - dateparser.parse(args.repeat)
        start_date = dateparser.parse(args.start_repeat_at) - delta

        task.status = Status.REPEAT.value
        task_id = actions.add_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE), task).id
        plan = Planner(
            task_id=task_id,
            user_id=user.id,
            interval=interval,
            last_repeat=start_date)
        actions.add_planner(
            PlannerMediator(user.id, config.DEFAULT_DATABASE), plan)
        print('Planned task added.')
    else:
        actions.add_task(
            TaskMediator(user.id, config.DEFAULT_DATABASE), task)
        print('Task added.')


def search_await_notifications(user):
    notifications = actions.search_notification_by_state(
        NotificationMediator(
            user.id, config.DEFAULT_DATABASE), State.AWAIT)

    if notifications:
        print('Notifications')
        for notification in notifications:
            actions.set_shown_notifications(
                NotificationMediator(
                    user.id, config.DEFAULT_DATABASE), notification.id)
            print_notification(notification)
