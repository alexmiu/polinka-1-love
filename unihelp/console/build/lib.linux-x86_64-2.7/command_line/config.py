import os
import logging


DIRECTORY_APP = os.path.join(os.environ["HOME"], "unihelp")
DEFAULT_DATABASE = os.path.join(DIRECTORY_APP, "unihelp.db")

LOGGING_ENABLED = True
LOG_LEVEL = logging.DEBUG
LOGS_DIRECTORY = DIRECTORY_APP
LOG_FILE = os.path.join(DIRECTORY_APP, "logging.log")
LOG_FORMAT = "%(asctime)s - %(name)s - [%(levelname)s] - %(message)s"
