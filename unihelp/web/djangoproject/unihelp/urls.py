from django.conf.urls import include, url
from django.contrib.auth.views import logout
from . import views


app_name = "unihelp"

users_patterns = [
    url(r'^$', views.users, name='users'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', logout, {'next_page': 'unihelp:home'}, name='logout'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^send/(?P<user_id>[0-9]+)/$', views.send_notification, name='send_notification'),
    url(r'^delete/(?P<user_id>[0-9]+)/$', views.delete_user, name='delete_user')
]


tags_patterns = [
    url(r'^$', views.tags, name='tags'),
    url(r'^new/$', views.create_tag, name='new_tag'),
    url(r'^edit/(?P<tag_id>[0-9]+)/$', views.edit_tag, name='edit_tag'),
    url(r'^delete/(?P<tag_id>[0-9]+)/$', views.delete_tag, name='delete_tag'),
]


planners_patterns = [
    url(r'^repeats/$', views.repeats, name='repeats'),
    url(r'^repeats/new$', views.create_repeat_task, name='new_repeat'),
    url(r'^$', views.planners, name='planners'),
    url(r'^new/$', views.create_planner, name='new_planner'),
    url(r'^edit/(?P<planner_id>[0-9]+)/$', views.edit_planner, name='edit_planner'),
    url(r'^delete/(?P<planner_id>[0-9]+)/$', views.delete_planner, name='delete_planner'),
]


notifications_patterns = [
    url(r'^$', views.notifications, name='notifications'),
    url(r'^new/$', views.create_notification, name='new_notification'),
    url(r'^all/$', views.search_all_notifications, name='all_notifications'),
    url(r'^created/$', views.search_created_notifications, name='created_notifications'),
    url(r'^await/$', views.search_await_notifications, name='await_notifications'),
    url(r'^shown/$', views.search_shown_notifications, name='shown_notifications'),
    url(r'^edit/(?P<notification_id>[0-9]+)/$', views.edit_notification, name='edit_notification'),
    url(r'^delete/(?P<notification_id>[0-9]+)/$', views.delete_notification, name='delete_notification'),
    url(r'^set_shown/(?P<notification_id>[0-9]+)/$', views.set_shown_notification, name='set_shown_notification'),
]


tasks_patterns = [
    url(r'^$', views.search_active_tasks, name='tasks'),
    url(r'^new/$', views.create_task, name='new_task'),
    url(r'^subtask/(?P<parent_id>[0-9]+)/$', views.add_subtask, name='add_subtask'),
    url(r'^(?P<task_id>[0-9]+)/$', views.show_task, name='show_task'),
    url(r'^edit/(?P<task_id>[0-9]+)/$', views.edit_task, name='edit_task'),
    url(r'^update/(?P<task_id>[0-9]+)/$', views.update_task, name='update_task'),
    url(r'^delete/(?P<task_id>[0-9]+)/$', views.delete_task, name='delete_task'),
    url(r'^delete_archived/(?P<task_id>[0-9]+)/$', views.delete_archived_task, name='delete_archived_task'),
    url(r'^subtask/$', views.user_tasks, name='user_tasks'),
    url(r'^can_read/$', views.search_can_read_tasks, name='can_read_tasks'),
    url(r'^can_write/$', views.search_can_write_tasks, name='can_write_tasks'),
    url(r'^tag/(?P<tag_id>[0-9]+)/$', views.search_tasks_by_tag, name='tasks_by_tag'),
    url(r'^status/(?P<status_value>[0-9]+)/$', views.search_tasks_by_status, name='tasks_by_status'),
    url(r'^priority/(?P<priority_value>[0-9]+)/$', views.search_tasks_by_priority, name='tasks_by_priority'),
    url(r'^planner/(?P<planner_id>[0-9]+)/$', views.search_tasks_by_planner, name='tasks_by_planner'),
]


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^users/', include(users_patterns)),
    url(r'^tags/', include(tags_patterns)),
    url(r'^tasks/', include(tasks_patterns)),
    url(r'^notifications/', include(notifications_patterns)),
    url(r'^planners/', include(planners_patterns))
]
