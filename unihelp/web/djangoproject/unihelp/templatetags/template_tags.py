import datetime

from django import template
from django.conf import settings

from unihelplib.actions import Actions
from unihelplib.models.models import (
    Status,
    State,
    Priority)


"""Load library of tags."""
register = template.Library()


@register.simple_tag
def search_tag_name(user_id, tag_id):
    actions = Actions(user_id, settings.UNIHELP_DATABASE)
    tag = actions.search_tag(tag_id)
    if tag is None:
        return tag
    return tag.name


@register.simple_tag
def get_status(status):
    return Status(status).name


@register.simple_tag
def get_priority(priority):
    return Priority(priority).name


@register.simple_tag
def get_status_tag(status):
    classes = {'CREATED': 'secondary',
               'IN_PROGRESS': 'primary',
               'DONE': 'success',
               'REPEAT': 'info',
               'ARCHIVED': 'warning'}
    return classes.get(Status(status).name, "")


@register.simple_tag
def get_priority_tag(priority):
    classes = {'MIN': 'secondary',
               'MID': 'primary',
               'MAX': 'danger'}
    return classes.get(Priority(priority).name, "")


@register.simple_tag
def get_task_title(user_id, task_id):
    actions = Actions(user_id, settings.UNIHELP_DATABASE)
    task = actions.search_task(task_id)
    if task is None:
        return task
    return task.title


@register.simple_tag
def get_state(state):
    return State(state).name


@register.simple_tag
def get_state_tag(status):
    classes = {'CREATED': 'secondary',
               'AWAIT': 'primary',
               'SHOWN': 'success'}
    return classes.get(State(status).name, "")


@register.simple_tag
def get_timedelta(seconds):
    return datetime.timedelta(seconds=seconds)
