from django.apps import AppConfig


class UnihelpAppConfig(AppConfig):
    name = 'unihelp'
