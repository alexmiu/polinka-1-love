import datetime
from tempus_dominus.widgets import DateTimePicker

from django import forms
from django.conf import settings
from django.contrib.auth.models import User

from unihelplib.actions import Actions
from unihelplib.models.models import (
    Status,
    Priority)


class TagForm(forms.Form):
    name = forms.CharField(max_length=200)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=200)
    password = forms.CharField(widget=forms.PasswordInput)


class TaskForm(forms.Form):
    def __init__(self, user_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        actions = Actions(user_id, settings.UNIHELP_DATABASE)

        tags = actions.search_user_tags()
        tags_tuple = [(tag.id, tag.name) for tag in tags]
        tags_tuple.insert(0, ('', '-------'))
        self.fields['tag'] = forms.ChoiceField(
            choices=tags_tuple, required=False)

    title = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'placeholder': 'Magic title!'}))
    note = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Magic note!'}))
    tag = forms.ChoiceField(required=False)
    status = forms.ChoiceField(
        choices=[(status.value, Status(status).name) for status in Status])
    priority = forms.ChoiceField(
        choices=[(priority.value, Priority(priority).name) for priority in Priority])
    event = forms.BooleanField(initial=False, required=False)
    time_start = forms.DateTimeField(
        widget=DateTimePicker(options={
            'minDate': (datetime.date.today()).strftime('%Y-%m-%d'),
            'useCurrent': True}),
        required=False)
    time_end = forms.DateTimeField(
        widget=DateTimePicker(options={
            'minDate': (
                    datetime.date.today()
                    + datetime.timedelta(days=1)).strftime('%Y-%m-%d'),
            'useCurrent': False}),
        required=False)
    readers = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False)
    writers = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False)


class SubtaskForm(TaskForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class NotificationForm(forms.Form):
    def __init__(self, user_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        actions = Actions(user_id, settings.UNIHELP_DATABASE)
        tasks = actions.search_tasks_not_repeat()
        tasks_tuple = [(task.id, task.title) for task in tasks]
        tasks_tuple.insert(0, ('', '-------'))
        self.fields['task'] = forms.ChoiceField(
            choices=tasks_tuple,
            required=False)

    title = forms.CharField(max_length=200)
    task = forms.ChoiceField(required=False)
    work_start_time = forms.DateTimeField(
        widget=DateTimePicker(options={
            'minDate': (datetime.date.today()).strftime('%Y-%m-%d'),
            'useCurrent': True}),
        required=False)


class MessageForm(NotificationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('task')


class RepeatTaskForm(TaskForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields.pop('status')


class PlannerForm(forms.Form):
    def __init__(self, user_id=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        actions = Actions(user_id, settings.UNIHELP_DATABASE)
        tasks = actions.search_tasks_by_status_(Status.REPEAT.value)
        tasks_tuple = [(task.id, task.title) for task in tasks]
        self.fields['task_repeat'] = forms.ChoiceField(choices=tasks_tuple)

    task_repeat = forms.ChoiceField(required=False)
    interval = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={'placeholder': '1 day'}))
    last_repeat = forms.DateTimeField(
        widget=DateTimePicker(options={
            'minDate': (datetime.date.today()).strftime('%Y-%m-%d'),
            'useCurrent': True}),
        required=False)
